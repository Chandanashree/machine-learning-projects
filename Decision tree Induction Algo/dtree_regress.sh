echo "This script produces all the results for fp growth  algorithm implementation"

path=$1
outdir=$2
sep=$3

echo $sep

fname_we=`basename ${path}`
fname=$(echo $fname_we | cut -f 1 -d '.')

echo ""
echo "Running Regression on: $fname_we"

head -n  100 ${path} > ${outdir}/${fname}_tup100_train.csv 
tail -n   20 ${path} > ${outdir}/${fname}_tup100_test.csv 
head -n  500 ${path} > ${outdir}/${fname}_tup500_train.csv 
tail -n  100 ${path} > ${outdir}/${fname}_tup500_test.csv 
head -n 1000 ${path} > ${outdir}/${fname}_tup1000_train.csv 
tail -n  200 ${path} > ${outdir}/${fname}_tup1000_test.csv 
head -n 3000 ${path} > ${outdir}/${fname}_tup3000_train.csv 
tail -n  600 ${path} > ${outdir}/${fname}_tup3000_test.csv 
head -n 6000 ${path} > ${outdir}/${fname}_tup6000_train.csv 
tail -n 1200 ${path} > ${outdir}/${fname}_tup6000_test.csv 

cut -d ${sep} -f1,2,3,4 < ${path} > ${outdir}/${fname}_dim4.csv
cut -d ${sep} -f1,2,3,4,5,6 < ${path} > ${outdir}/${fname}_dim6.csv
cut -d ${sep} -f1,2,3,4,5,6,7,8 < ${path} > ${outdir}/${fname}_dim8.csv
cut -d ${sep} -f1,2,3,4,5,6,7,8,9,10 < ${path} > ${outdir}/${fname}_dim10.csv
cut -d ${sep} -f1,2,3,4,5,6,7,8,9,10,11,12 < ${path} > ${outdir}/${fname}_dim12.csv

head -n 6000 ${outdir}/${fname}_dim4.csv > ${outdir}/${fname}_dim4_train.csv
tail -n 1200 ${outdir}/${fname}_dim4.csv > ${outdir}/${fname}_dim4_test.csv
head -n 6000 ${outdir}/${fname}_dim6.csv > ${outdir}/${fname}_dim6_train.csv
tail -n 1200 ${outdir}/${fname}_dim6.csv > ${outdir}/${fname}_dim6_test.csv
head -n 6000 ${outdir}/${fname}_dim8.csv > ${outdir}/${fname}_dim8_train.csv
tail -n 1200 ${outdir}/${fname}_dim8.csv > ${outdir}/${fname}_dim8_test.csv
head -n 6000 ${outdir}/${fname}_dim10.csv > ${outdir}/${fname}_dim10_train.csv
tail -n 1200 ${outdir}/${fname}_dim10.csv > ${outdir}/${fname}_dim10_test.csv
head -n 6000 ${outdir}/${fname}_dim12.csv > ${outdir}/${fname}_dim12_train.csv
tail -n 1200 ${outdir}/${fname}_dim12.csv > ${outdir}/${fname}_dim12_test.csv

echo Running tuples experiment, dimensions constant.
echo Running for tuple 100 ...
./dtree.exe ${outdir}/${fname}_tup100_train.csv ${outdir}/${fname}_tup100_test.csv  > ${outdir}/${fname}_tup100.log
tail -3 ${outdir}/${fname}_tup100.log
echo

echo Running for tuple 500 ...
./dtree.exe ${outdir}/${fname}_tup500_train.csv ${outdir}/${fname}_tup500_test.csv  > ${outdir}/${fname}_tup500.log
tail -3 ${outdir}/${fname}_tup500.log
echo

echo Running for tuple 1000 ...
./dtree.exe ${outdir}/${fname}_tup1000_train.csv ${outdir}/${fname}_tup1000_test.csv  > ${outdir}/${fname}_tup1000.log
tail -3 ${outdir}/${fname}_tup1000.log
echo

echo Running for tuple 3000 ...
./dtree.exe ${outdir}/${fname}_tup3000_train.csv ${outdir}/${fname}_tup3000_test.csv  > ${outdir}/${fname}_tup3000.log
tail -3 ${outdir}/${fname}_tup3000.log
echo

echo Running for tuple 6000 ...
./dtree.exe ${outdir}/${fname}_tup6000_train.csv ${outdir}/${fname}_tup6000_test.csv  > ${outdir}/${fname}_tup6000.log
tail -3 ${outdir}/${fname}_tup6000.log
echo

echo Running dimension experiment, keeping tuples constant.
echo Running for dimensions 4 ...
./dtree.exe ${outdir}/${fname}_dim4_train.csv  ${outdir}/${fname}_dim4_test.csv > ${outdir}/${fname}_dim4.log
tail -3 ${outdir}/${fname}_dim4.log
echo

echo Running for dimensions 6 ...
./dtree.exe ${outdir}/${fname}_dim6_train.csv  ${outdir}/${fname}_dim6_test.csv > ${outdir}/${fname}_dim6.log
tail -3 ${outdir}/${fname}_dim6.log
echo

echo Running for dimensions 8 ...
./dtree.exe ${outdir}/${fname}_dim8_train.csv  ${outdir}/${fname}_dim8_test.csv > ${outdir}/${fname}_dim8.log
tail -3 ${outdir}/${fname}_dim8.log
echo

echo Running for dimensions 10 ...
./dtree.exe ${outdir}/${fname}_dim10_train.csv ${outdir}/${fname}_dim10_test.csv > ${outdir}/${fname}_dim10.log
tail -3 ${outdir}/${fname}_dim10.log
echo

echo Running for dimensions 12 ...
./dtree.exe ${outdir}/${fname}_dim12_train.csv ${outdir}/${fname}_dim12_test.csv > ${outdir}/${fname}_dim12.log
tail -3 ${outdir}/${fname}_dim12.log
echo
