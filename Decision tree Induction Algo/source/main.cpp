#include <ctime>
#include "../include/input_proc.hpp"
#include "../include/dtree.hpp"

using std::string;
using std::vector;
using std::map;
using std::cout;
using std::endl;
using std::pair;
using std::set;

int mem_cnt;
vector<string> dims;

void print_help(const char* pname)
{
    cout << "Usage: " << string(pname) << " <Training File> <Test File>" << endl;
}

int main(int argc, char *argv[])
{
    vector<vector<string> > train_data;
    vector<vector<string> > test_data;

    if(argc < 3) {
        cout << "Invalid Number of Arguments!" << endl;
        print_help(argv[0]);
        exit(1);
    }
    else {
        cout << "Running Decision Tree with:" << endl;
        cout << "Training File: " << argv[1] << endl;
        cout << "Test File: " << argv[2] << endl;
    }
    
    train_data = read_csv(argv[1]);
    test_data = read_csv(argv[2]);

    int num_iter = 20;
    float train_secs = 0;
    float test_secs = 0;
    
    DTTree dttree;
    
    cout << "\nRunning Decision Tree Training ... " << endl;
    for(int i = 0; i < num_iter; i++) {
    	clock_t t1, t2;
    	t1 = clock();
    	dttree.train(train_data);
    	t2 = clock();
    	float diff ((float)t2 - (float)t1);
    	train_secs += diff / CLOCKS_PER_SEC;
    }
    cout << "... Done" << endl << endl;
    
    cout << "\nRunning Decision Tree Testing ... " << endl;
    clock_t t1, t2;
    t1 = clock();
    dttree.test(test_data);
    t2 = clock();
    float diff ((float)t2 - (float)t1);
    test_secs += diff / CLOCKS_PER_SEC;
    cout << "... Done" << endl << endl;
    
    cout << "Training Time: " << (train_secs * 1000)/num_iter << " ms" << endl;
    cout << "Testing Time: " << (test_secs * 1000)/num_iter << " ms" << endl;
    cout << "Memory Usage: " << mem_cnt/(1024 * num_iter) << " kb" << endl;
    
    return 0;
}
