import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm
from sklearn.neural_network import MLPClassifier
import sys
if(len(sys.argv)!=3):
    print("please input the path to data folder and 'vm' or 'nn' which ever classifier you want to use")
    sys.exit()
#input_data ='/home/haindavi/learning_systems/data/bank_data/1year.arff' 
input_data = sys.argv[1]

f_in = open(input_data)
num_data = 0
attr_names = []
f_in.readline()
f_in.readline()
data_start = 0
data = []
labels = []
#print("rand" ,len(test_indices))
test_data = []
test_labels = []
i=0

data_start = 1
for line in f_in:
    if(data_start ==1):
        num_data+=1
    if(data_start == 0):
        data_start = 1

#    words = line.strip().split(' ')
#    if(words[0][1:] == 'data'):
#        data_start = 1
    
data_start = 0
print(num_data)
test_indices = np.random.randint(num_data,size =int(0.2*num_data))
f_in = open(input_data)
for line in f_in:
    if(data_start == 0):
        words = line.strip().split(' ')
        for word in words:
            attr_names.append(word)

        #if(words[0][1:] == 'attribute'):
        #    attr_names.append(words[1])
        #if(words[0][1:] == 'data'):
        data_start = 1
        continue   
    if(data_start == 1):
        words = line.strip().split(',')
        lbl = int(words.pop(-1))
        for k in range(len(words)):
            if words[k] == '?':
                words[k] = "-1"

        if(i in test_indices):
            test_data.append([float(word) for word in words])
            test_labels.append(lbl)
        else:
            data.append([float(word) for word in words]) 
            labels.append(lbl)
        i += 1
#print(attr_names)
#print(len(data),len(test_data))
data = np.asarray(data)
labels = np.asarray(labels)
test_data = np.asarray(test_data)
test_labels = np.asarray(test_labels)
if(sys.argv[2]=='svm'):
    clf = svm.SVC()
if(sys.argv[2]=='nn'):
    clf = MLPClassifier(solver='lbfgs', alpha=1e-5,hidden_layer_sizes=(8), random_state=1)
clf.fit(data,labels)
predicted = clf.predict(test_data)
print(predicted)
accuracy = 0
for k in range(len(test_data)):
    if(test_labels[k] == predicted[k]):
        accuracy +=1

print("num of correct predictions = ",accuracy ,"percentage = ",accuracy*100/len(test_labels))
