#include "../include/dtree.hpp"

using std::pair;
using std::set;
using std::cout;
using std::endl;

extern int mem_cnt;

DTTree::DTTree() {
    root = new DTNode;
}

void DTTree::train(vector<vector<string> > train_data) {
    table_info = gen_table_info(train_data);
    gen_dtree(root, train_data, table_info);
    ms_class = get_frequent_class(train_data);
}

void DTTree::test(vector<vector<string> > test_data) {

    vector<string> golden_cl;
    vector<string> predict_cl;
    double accuracy = 0.0;
    
    for(int i = 1; i < test_data.size(); i++) {
        string label = test_data[i].back();
        golden_cl.push_back(label);
    }
    
    for(int i = 1; i < test_data.size(); i++) {
        string label = predict(root, test_data[i]);
        predict_cl.push_back(label);
    }
    
    accuracy = calculate_accuracy(golden_cl, predict_cl);
    
    cout << "Accuracy: " << accuracy << "%" << endl;
}

void DTTree::gen_dtree(DTNode* root, vector<vector<string> > table, vector<vector<string> > table_info) {
    
    if(table.size() <= 1) {
        return;
    }
    else if(are_same_class(table)) {
        root->is_leaf = true;
        root->split = "";
        root->label = table[1].back();
    }
    else {
        string split = get_splitting(table);
        root->split = split;
        root->is_leaf = false;
        int col_idx = get_col_idx(split, table_info);
        for(int i = 1; i < table_info[col_idx].size(); i++) {
            DTNode * newNode = (DTNode *) new DTNode;
            newNode->label = table_info[col_idx][i];
            newNode->is_leaf = false;
            newNode->split = split;
            root->children_vals.push_back(table_info[col_idx][i]);
            vector<vector<string> > pruned_table = prune_table(table, split, table_info[col_idx][i]);
            gen_dtree(newNode, pruned_table, table_info);
            root->children.push_back(newNode);
            mem_cnt += sizeof(DTNode);
            mem_cnt += sizeof(pruned_table);
        }
    }
}

vector<vector<string> > DTTree::gen_table_info(vector<vector<string> > table) {
    
    vector<vector<string> > table_info;
    
    for(int i = 0; i < table[0].size(); i++) {
        vector<string> t_info;
        map<string, int> t_map;
        for(int j = 0; j < table.size(); j++) {
            if(t_map.count(table[j][i]) == 0) {
                t_map[table[j][i]] = 1;
                t_info.push_back(table[j][i]);
            }
            else {
                t_map[table[j][i]]++;
            }
        }
        table_info.push_back(t_info);
    }
    
    return table_info;
}

int DTTree::get_col_idx(string split, vector<vector<string> > table_info) {
    for(int i = 0; i < table_info.size(); i++) {
        if(!table_info[i][0].compare(split)) {
            return i;
        }
    }
    return -1;
}

bool DTTree::are_same_class(vector<vector<string> > table) {
    string val = table[1].back();
    for(int i = 1; i < table.size(); i++) {
        if(table[i].back().compare(val))
            return false;
    }
    return true;
}

vector<vector<string> > DTTree::prune_table(vector<vector<string> > table, string split, string value) {

    int col = -1;
    vector<vector<string> > pruned_table;
    vector<string> header_row;
    
    for(int i = 0; i < table[0].size(); i++) {
        if(!table[0][i].compare(split)) {
            col = i;
            break;
        }
    }
    
    for(int i = 0; i < table[0].size(); i++) {
        if(i != col) {
            header_row.push_back(table[0][i]);
        }
    }
    
    if(header_row.size() != 0) pruned_table.push_back(header_row);
    
    for(int i = 0; i < table.size(); i++) {
        vector<string> row;
        if(!table[i][col].compare(value)) {
            for(int j = 0; j < table[i].size(); j++) {
                if(j != col) {
                    row.push_back(table[i][j]);
                }
            }
            if(row.size() != 0) pruned_table.push_back(row);
        }
    }
    
    return pruned_table;
}

string DTTree::get_splitting(vector<vector<string> > table) {
    
    int split = 0;
    
    double min_entropy = DBL_MAX;
    
    for(int i = 0; i < table[0].size()-1; i++) {
        string attr = table[0][i];
        map <string, int> attr_map;
        vector<int> counts = cnt_attr(table, i);
        vector<double> attr_entropy;
        double col_entropy = 0.0;
        
        for(int j = 1; j < table.size(); j++) {
            double entropy = 0.0;
            if(attr_map.find(table[j][i]) != attr_map.end()) {
                attr_map[table[j][i]]++;
            }
            else {
                attr_map[table[j][i]] = 1;
                vector<vector<string> > pruned_table = prune_table(table, attr, table[j][i]);
                vector<int> class_cnts = cnt_attr(pruned_table, pruned_table[0].size()-1);
                
                for(int k = 0; k < class_cnts.size(); k++) {
                    double temp = (double) class_cnts[k];
                    entropy -= (temp/class_cnts[class_cnts.size()-1]) * (log(temp/class_cnts[class_cnts.size()-1]) / log(2));
                }
                
                attr_entropy.push_back(entropy);
                entropy = 0.0;
            }
        }
        
        for(int k = 0; k < counts.size()-1; k++) {
            col_entropy += ((double) counts[k] * (double) attr_entropy[k]);
        }
        
        col_entropy = col_entropy / (double) counts[counts.size() - 1];
        
        if(col_entropy <= min_entropy) {
            min_entropy = col_entropy;
            split = i;
        }
    }
    
    return table[0][split];
}

vector<int> DTTree::cnt_attr(vector<vector<string> > table, unsigned long idx) {
    vector<string> attr_vec;
    vector<int> counts;
    
    bool present = false;
    int pidx = 0;
    int sum = 0;
    
    for(int i = 1; i < table.size(); i++) {
        for(int j = 0; j < attr_vec.size(); j++) {
            if(!attr_vec[j].compare(table[i][idx])) {
                present = true;
                pidx = j;
                break;
            }
            else {
                present = false;
            }
        }
        
        if(!present) {
            counts.push_back(1);
            attr_vec.push_back(table[i][idx]);
        }
        else {
            counts[pidx]++;
        }
    }
    
    for(auto cnt : counts) {
        sum += cnt;
    }
    
    counts.push_back(sum);
    return counts;
}

string DTTree::predict(DTNode *root, vector<std::string> attrs) {
    string prediction;
    
    while(!root->is_leaf && !root->children.empty()) {
        int idx = get_col_idx(root->split, table_info);
        string val = attrs[idx];
        int cidx = -1;
        for(int i = 0; i < root->children_vals.size(); i++) {
            if(!root->children_vals[i].compare(val)) {
                cidx = i;
            }
        }
        if(cidx == -1) {
            prediction = ms_class;
            break;
        }
        root = root->children[cidx];
        if(root == NULL) {
            prediction = ms_class;
            break;
        }
        prediction = root->label;
    }
    
    if(!root->is_leaf) prediction = ms_class;
    
    return prediction;
}

double DTTree::calculate_accuracy(vector<std::string> golden, vector<std::string> predictions) {
    int passed = 0;
    
    for(int i = 0; i < golden.size(); i++) {
        if(!golden[i].compare(predictions[i])) {
            passed++;
        }
        cout << "Golden: " << golden[i] << ", Predicted: " << predictions[i] << endl;
    }
    
    return (double) passed/golden.size() * 100;
}

string DTTree::get_frequent_class(vector<vector<std::string> > table) {
   
    map<string, int> t_class;
    string f_class;
    int h_cnt = 0;
    
    for(int i = 1; i < table.size(); i++) {
        if(t_class.count(table[i][table[0].size()-1]) == 0) {
            t_class[table[i].back()] = 1;
        }
        else {
            t_class[table[i].back()]++;
        }
    }
    
    map<string, int>::iterator iter;
    
    for(iter = t_class.begin(); iter != t_class.end(); iter++) {
        if(iter->second >= h_cnt) {
            h_cnt = iter->second;
            f_class = iter->first;
        }
    }
    
    return f_class;
}
