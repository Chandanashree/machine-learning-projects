#ifndef tree_hpp
#define tree_hpp

#include <iostream>
#include <string>
#include <map>
#include <set>
#include <functional>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <cstring>
#include <cstdlib>
#include <cfloat>
#include <cmath>

using std::string;
using std::vector;
using std::map;

class DTNode {
public:
    string           label;
    string           split;
    int              is_leaf;
    vector <DTNode*> children;
    vector <string>  children_vals;
};

class DTTree {
public:
    DTNode* root;
    string ms_class;
    vector<vector<string> > table_info;
    
    DTTree();
    void train(vector<vector<string> > table);
    void test(vector<vector<string> > table);
    void gen_dtree(DTNode* root, vector<vector<string> > table, vector<vector<string> > table_info);
    bool are_same_class(vector<vector<string> > table);
    string get_splitting(vector<vector<string> > table);
    vector<vector<string> > gen_table_info(vector<vector<string> > table);
    vector<vector<string> > prune_table(vector<vector<string> > table, string split, string val);
    int get_col_idx(string split, vector<vector<string> > table_info);
    vector<int> cnt_attr(vector<vector<string> > table, unsigned long idx);
    string get_frequent_class(vector<vector<string> > table);
    string predict(DTNode* root, vector<string> attrs);
    double calculate_accuracy(vector<string> golden, vector<string> predictions);
};

#endif /* tree_hpp */
