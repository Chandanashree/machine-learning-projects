echo "This script produces all the results for fp growth  algorithm implementation"

path=$1
outdir=$2
sep=$3

echo $sep

fname_we=`basename ${path}`
fname=$(echo $fname_we | cut -f 1 -d '.')

echo ""
echo "Running Regression on: $fname_we"

head -n  100 ${path} > ${outdir}/${fname}_tup100_train.csv 
tail -n   20 ${path} > ${outdir}/${fname}_tup100_test.csv 
head -n  200 ${path} > ${outdir}/${fname}_tup200_train.csv 
tail -n   40 ${path} > ${outdir}/${fname}_tup200_test.csv 
head -n  400 ${path} > ${outdir}/${fname}_tup400_train.csv 
tail -n   80 ${path} > ${outdir}/${fname}_tup400_test.csv 
head -n  800 ${path} > ${outdir}/${fname}_tup800_train.csv 
tail -n  160 ${path} > ${outdir}/${fname}_tup800_test.csv 
head -n 1600 ${path} > ${outdir}/${fname}_tup1600_train.csv 
tail -n  320 ${path} > ${outdir}/${fname}_tup1600_test.csv 

cut -d ${sep} -f1,2 < ${path} > ${outdir}/${fname}_dim2.csv
cut -d ${sep} -f1,2,3,4 < ${path} > ${outdir}/${fname}_dim4.csv
cut -d ${sep} -f1,2,3,4,5,6 < ${path} > ${outdir}/${fname}_dim6.csv
cut -d ${sep} -f1,2,3,4,5,6,7,8 < ${path} > ${outdir}/${fname}_dim8.csv

head -n 1600 ${outdir}/${fname}_dim2.csv > ${outdir}/${fname}_dim2_train.csv
tail -n 320 ${outdir}/${fname}_dim2.csv > ${outdir}/${fname}_dim2_test.csv
head -n 1600 ${outdir}/${fname}_dim4.csv > ${outdir}/${fname}_dim4_train.csv
tail -n 320 ${outdir}/${fname}_dim4.csv > ${outdir}/${fname}_dim4_test.csv
head -n 1600 ${outdir}/${fname}_dim6.csv > ${outdir}/${fname}_dim6_train.csv
tail -n 320 ${outdir}/${fname}_dim6.csv > ${outdir}/${fname}_dim6_test.csv
head -n 1600 ${outdir}/${fname}_dim8.csv > ${outdir}/${fname}_dim8_train.csv
tail -n 320 ${outdir}/${fname}_dim8.csv > ${outdir}/${fname}_dim8_test.csv

echo Running tuples experiment, dimensions constant.
echo Running for tuple 100 ...
./dtree.exe ${outdir}/${fname}_tup100_train.csv ${outdir}/${fname}_tup100_test.csv  > ${outdir}/${fname}_tup100.log
tail -3 ${outdir}/${fname}_tup100.log
echo

echo Running for tuple 200 ...
./dtree.exe ${outdir}/${fname}_tup200_train.csv ${outdir}/${fname}_tup200_test.csv  > ${outdir}/${fname}_tup200.log
tail -3 ${outdir}/${fname}_tup200.log
echo

echo Running for tuple 400 ...
./dtree.exe ${outdir}/${fname}_tup400_train.csv ${outdir}/${fname}_tup400_test.csv  > ${outdir}/${fname}_tup400.log
tail -3 ${outdir}/${fname}_tup400.log
echo

echo Running for tuple 800 ...
./dtree.exe ${outdir}/${fname}_tup800_train.csv ${outdir}/${fname}_tup800_test.csv  > ${outdir}/${fname}_tup800.log
tail -3 ${outdir}/${fname}_tup800.log
echo

echo Running for tuple 1600 ...
./dtree.exe ${outdir}/${fname}_tup1600_train.csv ${outdir}/${fname}_tup1600_test.csv  > ${outdir}/${fname}_tup1600.log
tail -3 ${outdir}/${fname}_tup1600.log
echo

echo Running dimension experiment, keeping tuples constant.
echo Running for dimensions 2 ...
./dtree.exe ${outdir}/${fname}_dim2_train.csv  ${outdir}/${fname}_dim2_test.csv > ${outdir}/${fname}_dim2.log
tail -3 ${outdir}/${fname}_dim2.log
echo

echo Running for dimensions 4 ...
./dtree.exe ${outdir}/${fname}_dim4_train.csv  ${outdir}/${fname}_dim4_test.csv > ${outdir}/${fname}_dim4.log
tail -3 ${outdir}/${fname}_dim4.log
echo

echo Running for dimensions 6 ...
./dtree.exe ${outdir}/${fname}_dim6_train.csv  ${outdir}/${fname}_dim6_test.csv > ${outdir}/${fname}_dim6.log
tail -3 ${outdir}/${fname}_dim6.log
echo

echo Running for dimensions 8 ...
./dtree.exe ${outdir}/${fname}_dim8_train.csv  ${outdir}/${fname}_dim8_test.csv > ${outdir}/${fname}_dim8.log
tail -3 ${outdir}/${fname}_dim8.log
echo
