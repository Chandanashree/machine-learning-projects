echo "This script produces all the results for fp growth  algorithm implementation"

path=$1
outdir=$2
sep=$3

echo $sep

fname_we=`basename ${path}`
fname=$(echo $fname_we | cut -f 1 -d '.')

echo ""
echo "Running Regression on: $fname_we"

head -n  100 ${path} > ${outdir}/${fname}_tup100.csv 
head -n  500 ${path} > ${outdir}/${fname}_tup500.csv 
head -n 1000 ${path} > ${outdir}/${fname}_tup1000.csv 
head -n 3000 ${path} > ${outdir}/${fname}_tup3000.csv 
head -n 6000 ${path} > ${outdir}/${fname}_tup6000.csv 
head -n 9000 ${path} > ${outdir}/${fname}_tup9000.csv 

cut -d ${sep} -f1,2,3,4 < ${path} > ${outdir}/${fname}_dim4.csv
cut -d ${sep} -f1,2,3,4,5,6 < ${path} > ${outdir}/${fname}_dim6.csv
cut -d ${sep} -f1,2,3,4,5,6,7,8 < ${path} > ${outdir}/${fname}_dim8.csv
cut -d ${sep} -f1,2,3,4,5,6,7,8,9,10 < ${path} > ${outdir}/${fname}_dim10.csv
cut -d ${sep} -f1,2,3,4,5,6,7,8,9,10,11,12 < ${path} > ${outdir}/${fname}_dim12.csv
cut -d ${sep} -f1,2,3,4,5,6,7,8,9,10,11,12,13,14 < ${path} > ${outdir}/${fname}_dim14.csv

echo Running min_sub experiment, keeping tuples and dimensions constant.
echo Running for minsub 20 ...
./fpg.exe ${path} 20 > ${outdir}/${fname}_minsup20.log
tail -2 ${outdir}/${fname}_minsup20.log
echo 
echo Running for minsub 40 ...
./fpg.exe ${path} 40 > ${outdir}/${fname}_minsup40.log
tail -2 ${outdir}/${fname}_minsup40.log
echo 
echo Running for minsub 80 ...
./fpg.exe ${path} 80 > ${outdir}/${fname}_minsup80.log
tail -2 ${outdir}/${fname}_minsup80.log
echo 
echo Running for minsub 160 ...
./fpg.exe ${path} 160 > ${outdir}/${fname}_minsup160.log
tail -2 ${outdir}/${fname}_minsup160.log
echo 
echo Running for minsub 320 ...
./fpg.exe ${path} 320 > ${outdir}/${fname}_minsup320.log
tail -2 ${outdir}/${fname}_minsup320.log
echo 
echo Running for minsub 640 ...
./fpg.exe ${path} 640 > ${outdir}/${fname}_minsup640.log
tail -2 ${outdir}/${fname}_minsup640.log
echo 

echo Running tuples experiment, keeping min_sup and dimensions constant.
echo Running for tuple 100 ...
./fpg.exe ${outdir}/${fname}_tup100.csv 20 > ${outdir}/${fname}_tup100.log

tail -2 ${outdir}/${fname}_tup100.log
echo

echo Running for tuple 500 ...
./fpg.exe ${outdir}/${fname}_tup500.csv 20 > ${outdir}/${fname}_tup500.log
tail -2 ${outdir}/${fname}_tup500.log
echo

echo Running for tuple 1000 ...
./fpg.exe ${outdir}/${fname}_tup1000.csv 20 > ${outdir}/${fname}_tup1000.log

tail -2 ${outdir}/${fname}_tup1000.log
echo

echo Running for tuple 3000 ...
./fpg.exe ${outdir}/${fname}_tup3000.csv 20 > ${outdir}/${fname}_tup3000.log
tail -2 ${outdir}/${fname}_tup3000.log
echo

echo Running for tuple 6000 ...
./fpg.exe ${outdir}/${fname}_tup6000.csv 20 > ${outdir}/${fname}_tup6000.log
tail -2 ${outdir}/${fname}_tup6000.log
echo

echo Running for tuple 9000 ...
./fpg.exe ${outdir}/${fname}_tup9000.csv 20 > ${outdir}/${fname}_tup9000.log
tail -2 ${outdir}/${fname}_tup9000.log
echo

echo Running dimension experiment, keeping tuples and min_sup constant.
echo Running for dimensions 4 ...
./fpg.exe ${outdir}/${fname}_dim4.csv 20 > ${outdir}/${fname}_dim4.log
tail -2 ${outdir}/${fname}_dim4.log
echo

echo Running for dimensions 6 ...
./fpg.exe ${outdir}/${fname}_dim6.csv 20 > ${outdir}/${fname}_dim6.log
tail -2 ${outdir}/${fname}_dim6.log
echo

echo Running for dimensions 8 ...
./fpg.exe ${outdir}/${fname}_dim8.csv 20 > ${outdir}/${fname}_dim8.log
tail -2 ${outdir}/${fname}_dim8.log
echo

echo Running for dimensions 10 ...
./fpg.exe ${outdir}/${fname}_dim10.csv 20 > ${outdir}/${fname}_dim10.log
tail -2 ${outdir}/${fname}_dim10.log
echo

echo Running for dimensions 12 ...
./fpg.exe ${outdir}/${fname}_dim12.csv 20 > ${outdir}/${fname}_dim12.log
tail -2 ${outdir}/${fname}_dim12.log
echo

echo Running for dimensions 14 ...
./fpg.exe ${outdir}/${fname}_dim14.csv 20 > ${outdir}/${fname}_dim14.log
tail -2 ${outdir}/${fname}_dim14.log
echo
