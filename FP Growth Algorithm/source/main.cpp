//
//  main.cpp
//  star_cubing
//
//  Created by Atif Ahangar on 3/18/18.
//  Copyright © 2018 Atif Ahangar. All rights reserved.
//

#include <ctime>
#include "../include/input_proc.hpp"
#include "../include/fp_tree.hpp"
#include "../include/fp_growth.hpp"

using std::string;
using std::vector;
using std::map;
using std::cout;
using std::endl;

int min_sup;
int mem_cnt;
vector<string> dims;

bool sortbysec(const pair<set<string>, int> &a, 
               const pair<set<string>, int> &b) 
{
    return (a.second > b.second);
}

void print_help(const char* pname)
{
    cout << "Usage: " << string(pname) << " <csv file> <min_sub>" << endl;
    cout << "<csv file>: Input data file." << endl;
    cout << "<min_sub>: Minimum Support." << endl;
}

int main(int argc, char *argv[])
{
    vector<vector<string> >  data;
    set<pair<set<string>, int> > frequent_patterns;
    
    if(argc < 3) {
        cout << "Invalid Number of Arguments!" << endl;
        print_help(argv[0]);
        exit(1);
    }
    else {
        cout << "Running FP growth  with:" << endl;
        cout << "Input Data file: " << argv[1] << endl;
        cout << "Minimum Support: " << argv[2] << endl;
    }
    
    min_sup = atoi(argv[2]);
    data = read_csv(argv[1]);
    
    FGTree fgtree(data, min_sup);

    int num_iter = 20;
    float secs = 0;
    
    cout << "\nRunning FP growth ... " << endl;
    for(int i = 0; i < num_iter; i++) {
    	clock_t t1, t2;
    	t1 = clock();
    	frequent_patterns = fp_growth(&fgtree);
    	t2 = clock();
    	float diff ((float)t2 - (float)t1);
    	secs += diff / CLOCKS_PER_SEC;
    }
    cout << "... Done" << endl << endl;
    
    /*
    for(auto pattern : frequent_patterns) {
        for(auto attr : pattern.first) {
            cout << attr << ", ";
        }
        cout << "- Count: " << pattern.second << endl;
    }
    */
    
    vector< pair< set<string>, int> > vect;

    for(auto pattern : frequent_patterns) {
        vect.push_back(pattern);
    }

    sort(vect.begin(), vect.end(), sortbysec);

    for(auto pattern : vect) {
        for(auto attr : pattern.first) {
            cout << attr << ", ";
        }
        cout << "- Count: " << pattern.second << endl;
    }
    
    cout << "\nRunning Time: " << (secs * 1000)/num_iter << " ms" << endl;
    cout << "Memory Usage: " << mem_cnt/(1024 * num_iter) << " kb" << endl;
    
    return 0;
}
