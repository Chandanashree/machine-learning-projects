//
//  input_proc.cpp
//  star_cubing
//
//  Created by Atif Ahangar on 3/18/18.
//  Copyright © 2018 Atif Ahangar. All rights reserved.
//

#include "../include/input_proc.hpp"

using std::ifstream;
using std::istringstream;
using std::cout;
using std::endl;

extern vector<string> dims;

vector<vector<string> > read_csv(const string &fname) {
    vector<vector<string> > data;
    
    string line, word;

    ifstream fcsv(fname.c_str());
    if(!fcsv) {
        cout << "Unable to read file: " << fname << endl;
        exit(1);
    }
    
    //parse header row.
    getline(fcsv, line);
    
    //finding separator.
    char sep = ';';
    if(line.find(';') != std::string::npos) {
        sep = ';';
    }
    else if(line.find(',') != std::string::npos) {
        sep = ',';
    }
    else if(line.find(' ') != std::string::npos) {
        sep = ' ';
    }
    else {
        cout << "Invalid separator!!" << endl;
        exit(1);
    }
    
    cout << "Using separator: " << sep << endl;
    
    istringstream ss(line);
    while(getline(ss, word, sep)) {
        dims.push_back(word);
    }
    
    //removing extra "\r" columns
    dims.pop_back();
    //dims.pop_back();
    
    //parse rest of the data
    while(getline(fcsv, line))
    {
        istringstream ss(line);
        vector<string> temp;
        int idx = 0;
        while(getline(ss, word, sep) && (idx < dims.size()))
        {
            temp.push_back(word);
            idx++;
        }
        data.push_back(temp);
    }
    
    return data;
}
