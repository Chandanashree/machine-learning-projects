//
//  tree.cpp
//  star_cubing
//
//  Created by Atif Ahangar on 3/18/18.
//  Copyright © 2018 Atif Ahangar. All rights reserved.
//

#include "../include/fp_tree.hpp"

using std::pair;
using std::set;

extern int mem_cnt;
    
FGNode::FGNode(string attr_a, FGNode* parent_a) {
    attr     = attr_a;
    count    = 1;
    parent   = parent_a;
    link     = NULL;
    mem_cnt += sizeof(FGNode);
}

FGTree::FGTree(vector<vector<string> > seqs, int min_sup_a) {
    root      = new FGNode(string{}, NULL);
    min_sup   = min_sup_a;
    mem_cnt  += sizeof(FGTree);
    
    map<string, int> cnt_data;
    
    for(vector<string> row : seqs) {
        for(string attr : row) {
            if(attr != "?") cnt_data[attr]++;
        }
    }
    
    for(auto it = cnt_data.begin(); it != cnt_data.end(); ) {
        if(it->second < min_sup) {
            cnt_data.erase(it++);
        }
        else {
            it++;
        }
    }
    
    struct comparator {
        bool operator() (const std::pair<int, string>& a, const std::pair<int, string>& b) const
        {
            return (a.first != b.first) ? a.first > b.first : a.second > b.second;
        }
    };
    
    set<pair<int, string>, comparator> odr_cnt_data;
    
    for(auto pair : cnt_data) {
        odr_cnt_data.insert({pair.second, pair.first});
    }
    
    for(vector<string> row : seqs) {
        FGNode* curr_node = root;
        
        for(auto pair : odr_cnt_data) {
            string curr_attr = pair.second;
            
            if(std::find(row.begin(), row.end(), curr_attr) != row.end()) {
                const auto it = std::find_if(curr_node->children.begin(), curr_node->children.end() , [curr_attr](FGNode* fgnode) {return fgnode->attr == curr_attr;});

                if(it == curr_node->children.end()) {
                    auto new_child = new FGNode(curr_attr, curr_node);
                    curr_node->children.push_back(new_child);
                    
                    if(htable.count(new_child->attr)) {
                        auto prev_node = htable[new_child->attr];
                        while(prev_node->link) {
                            prev_node = prev_node->link;
                        }
                        prev_node->link = new_child;
                    }
                    else {
                        htable[new_child->attr] = new_child;
                    }
                    curr_node = new_child;
                }
                else {
                    auto curr_child = *it;
                    curr_child->count++;
                    curr_node = curr_child;
                }
            }
        }
    }
}
