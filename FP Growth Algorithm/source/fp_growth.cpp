#include <iostream>
#include <string>
#include <map>
#include <set>
#include <functional>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <cstring>
#include <cstdlib>

#include "../include/fp_tree.hpp"
#include "../include/input_proc.hpp"
#include "../include/fp_growth.hpp"

using std::string;
using std::vector;
using std::cout;
using std::endl;
using std::set;
using std::pair;

bool is_single_path(FGNode* fgnode) {
    if(fgnode->children.size() == 0) {
        return true;
    }
    else if(fgnode->children.size() > 1) {
        return false;
    }
    else {
        return is_single_path(fgnode->children.front());
    }
}

set<pair<set<string>, int>> fp_growth(FGTree* fgtree) {
    
    set<pair<set<string>, int>> fre_patterns;
    
    if(fgtree->root->children.size() == 0) {
        //cout << "Tree is empty" << endl;
        return {};
    }
    else if(is_single_path(fgtree->root)) {
        //cout << "Tree is single path" << endl;
        
        auto curr_node = fgtree->root->children.front();
        
        while(curr_node) {
            string curr_attr = curr_node->attr;
            int curr_cnt = curr_node->count;
            
            pair<set<string>, int> new_pattern{{curr_attr}, curr_cnt};
            fre_patterns.insert(new_pattern);
            
            for(pair<set<string>, int> pattern : fre_patterns) {
                pair<set<string>, int> new_pattern{pattern};
                new_pattern.first.insert(curr_attr);
                new_pattern.second = curr_cnt;
                
                fre_patterns.insert(new_pattern);
            }
            
            if(curr_node->children.size() == 1) {
                curr_node = curr_node->children.front();
            }
            else {
                curr_node = NULL;
            }
        }
    }
    else {
        //cout << "Tree is multi path" << endl;
        
        for(auto pair : fgtree->htable) {
            string curr_attr = pair.first;
            
            vector<std::pair<vector<string>, int>> cp_base;
            
            auto start_node = pair.second;
            while(start_node) {
                int start_cnt = start_node->count;
                auto curr_node = start_node->parent;
                
                if(curr_node->parent) {
                    std::pair<vector<string>, int> tp_path{{}, start_cnt};
                    while(curr_node->parent) {
                        tp_path.first.push_back(curr_node->attr);
                        curr_node = curr_node->parent;
                    }
                    
                    cp_base.push_back(tp_path);
                }
                
                start_node = start_node->link;
            }
            
            vector<vector<string>> cp_tans;
            for(std::pair<vector<string>, int> tp_path : cp_base) {
                vector<string> tp_attr = tp_path.first;
                int tp_freq = tp_path.second;
                
                for(auto i = 0; i < tp_freq; i++) {
                    cp_tans.push_back(tp_attr);
                }
            }
            
            FGTree c_fgtree(cp_tans, fgtree->min_sup);
            
            std::set<std::pair<std::set<string>, int>> cnd_patterns = fp_growth(&c_fgtree);
            
            std::set<std::pair<std::set<string>, int>> curr_patterns;
            
            int curr_cnt = 0;
            auto fg_node = pair.second;
            while(fg_node) {
                curr_cnt += fg_node->count;
                fg_node = fg_node->link;
            }
            
            std::pair<std::set<string>, int> pattern{{curr_attr}, curr_cnt};
            curr_patterns.insert(pattern);
            
            for(auto pattern : cnd_patterns) {
                std::pair<std::set<string>, int> new_pattern{pattern};
                new_pattern.first.insert(curr_attr);
                new_pattern.second = pattern.second;
                
                curr_patterns.insert({new_pattern});
            }
            
            fre_patterns.insert(curr_patterns.begin(), curr_patterns.end());
        }
        
    }
    
    return fre_patterns;
}
