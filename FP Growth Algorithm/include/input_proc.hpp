//
//  input_proc.hpp
//  star_cubing
//
//  Created by Atif Ahangar on 3/18/18.
//  Copyright © 2018 Atif Ahangar. All rights reserved.
//

#ifndef input_proc_hpp
#define input_proc_hpp

#include <iostream>
#include <string>
#include <map>
#include <set>
#include <functional>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <cstring>
#include <cstdlib>

using std::string;
using std::vector;
using std::map;

vector<vector<string> > read_csv(const string &fname);

#endif /* input_proc_hpp */
