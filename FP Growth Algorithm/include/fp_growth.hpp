//
//  star_cubing.h
//  star_cubing
//
//  Created by Atif Ahangar on 3/18/18.
//  Copyright © 2018 Atif Ahangar. All rights reserved.
//

#ifndef star_cubing_h
#define star_cubing_h

#include <map>
#include <set>
#include <string>
#include <vector>

using std::set;
using std::pair;

set<pair<set<string>, int>> fp_growth(FGTree* fgtree);

#endif /* star_cubing_h */
