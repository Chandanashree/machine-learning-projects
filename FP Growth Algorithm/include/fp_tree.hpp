//
//  tree.hpp
//  star_cubing
//
//  Created by Atif Ahangar on 3/18/18.
//  Copyright © 2018 Atif Ahangar. All rights reserved.
//

#ifndef tree_hpp
#define tree_hpp

#include <iostream>
#include <string>
#include <map>
#include <set>
#include <functional>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <cstring>
#include <cstdlib>

using std::string;
using std::vector;
using std::map;

class FGNode {
public:
    string           attr;
    string           dimen;
    int              count;
    FGNode*          link;
    FGNode*          parent;
    vector <FGNode*> children;
    
    FGNode(string attr, FGNode* node);
};

class FGTree {
public:
    FGNode*              root;
    map<string, FGNode*> htable;
    int                  min_sup;
    
    FGTree(vector<vector<string> > seqs, int min_sup);
};

#endif /* tree_hpp */
