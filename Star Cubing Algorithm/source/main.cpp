//
//  main.cpp
//  star_cubing
//
//  Created by Atif Ahangar on 3/18/18.
//  Copyright © 2018 Atif Ahangar. All rights reserved.
//

#include <ctime>
#include "../include/tree.hpp"
#include "../include/input_proc.hpp"
#include "../include/star_cubing.hpp"

using std::string;
using std::vector;
using std::map;
using std::cout;
using std::endl;

int tree_cnt;
int dim_sz;
int star_cnt;
int min_sub;
int mem_cnt;
vector<string> dims;
map<string, int> dims_idx;
Node* root;

void print_help(const char* pname)
{
    cout << "Usage: " << string(pname) << " <csv file> <min_sub> [separator] [log]" << endl;
    cout << "<csv file>: Input data file. The data should be separated using \';\', unless separator argument is used." << endl;
    cout << "min_sub: Iceberg condition." << endl;
    cout << "separator: Character used for separating data in Input data file. (Optional: \';\' by default)." << endl;
    cout << "log: Enable output of Cuboids. (Optional: True by default)" << endl;
}

int main(int argc, char *argv[])
{
    map <string, vector<string> >  data;
    map <string, map<string, int> > cnt_data;
    map <string, vector<string> > data_ib;
    map <int, vector<string> > table_ib;
    map <vector<string>, int > table_cmpx;
    map <string, int> output_cbs;
    char sep = ';';
    bool logging = true;
    
    if(argc < 3) {
        cout << "Invalid Number of Arguments!" << endl;
        print_help(argv[0]);
        exit(1);
    }
    else {
        cout << "Running Start Cubing with:" << endl;
        cout << "Input Data file: " << argv[1] << endl;
        cout << "min_sub: " << argv[2] << endl;
    }
    
    if(argc >= 4) sep = *argv[3];
    if(argc == 5) logging = atoi(argv[4]);
    
    cout << "separator: " << sep << endl;
    cout << "logging: " << logging << endl << endl;
    
    min_sub = atoi(argv[2]);
    
    data = read_csv(argv[1], sep);
    
    for(int i = 0; i < dims.size(); i++) {
        dims_idx[dims[i]] = i;
    }
    
    for(map<string, vector<string> >::iterator it = data.begin(); it != data.end(); ++it) {
        for(int i = 0; i < it->second.size(); i++) {
            if(cnt_data[it->first].find(it->second[i]) == cnt_data[it->first].end())
                cnt_data[it->first][it->second[i]] = 1;
            else
                cnt_data[it->first][it->second[i]]++;
        }
    }
    
    //Todo: can we use the data itself, replacing the values with * as needed.
    for(map<string, vector<string> >::iterator it = data.begin(); it != data.end(); ++it) {
        for(int i = 0; i < it->second.size(); i++) {
            if(cnt_data[it->first][it->second[i]] >= min_sub)
                data_ib[it->first].push_back(data[it->first][i]);
            else
                data_ib[it->first].push_back("* ");
        }
    }
    
    for(map<string, vector<string> >::iterator it = data_ib.begin(); it != data_ib.end(); ++it) {
        for(int i = 0; i < it->second.size(); i++) {
            table_ib[i].push_back(data_ib[it->first][i]);
        }
    }
    
    for(map<string, vector<string> >::iterator it = data_ib.begin(); it != data_ib.end(); ++it) {
        for(int i = 0; i < it->second.size(); i++) {
            table_ib[i].at(dims_idx[it->first]) = data_ib[it->first][i];
        }
    }
    
    dim_sz = (int) table_ib[0].size();
    
    for(map<int, vector<string> >::iterator it = table_ib.begin(); it != table_ib.end(); ++it) {
        if(table_cmpx.find(table_ib[it->first]) == table_cmpx.end()) {
            table_cmpx[table_ib[it->first]] = 1;
        }
        else {
            table_cmpx[table_ib[it->first]]++;
        }
    }
    
    //starCubingBF(table_cmpx);
    
    Tree *starTree = new Tree();
    starTree->tree_lvl = 0;
    
    root = starTree->genStarTree(table_cmpx, dims);
    
    tree_cnt = 0;
    star_cnt = 0;
    
    cout << "Running Star Cubing ... " << endl;
    clock_t t1, t2;
    t1 = clock();
    starCubing(starTree, root, output_cbs);
    t2 = clock();
    float diff ((float)t2 - (float)t1);
    float secs = diff / CLOCKS_PER_SEC;
    cout << "... Done" << endl << endl;
    
    for(map<string, int>::iterator it = output_cbs.begin(); it != output_cbs.end(); ++it) {
        cout << "Cuboid: " << it->first << " Count: " << it->second << endl;
    }
    cout << endl;
    
    cout << "Running Time: " << secs * 1000 << " ms" << endl;
    cout << "Memory Usage: " << mem_cnt/1024 << " kb" << endl;
    
    return 0;
}
