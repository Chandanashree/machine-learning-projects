//
//  input_proc.cpp
//  star_cubing
//
//  Created by Atif Ahangar on 3/18/18.
//  Copyright © 2018 Atif Ahangar. All rights reserved.
//

#include "../include/input_proc.hpp"

using std::ifstream;
using std::istringstream;
using std::cout;
using std::endl;

extern vector<string> dims;

map <string, vector<string> > read_csv(const string &fname, char sep) {
    map <string, vector<string> > data;
    
    string line, word;

    ifstream fcsv(fname.c_str());
    if(!fcsv) {
        cout << "Unable to read file: " << fname << endl;
        exit(1);
    }
    
    //parse header row.
    getline(fcsv, line);
    istringstream ss(line);
    while(getline(ss, word, sep)) {
        dims.push_back(word);
    }
    
    //TODO: removing extra "\r" columns
    dims.pop_back();
    dims.pop_back();
    
    //parse rest of the data
    while(getline(fcsv, line))
    {
        int idx = 0;
        istringstream ss(line);
        while(getline(ss, word, sep) && (idx < dims.size()))
        {
            data[dims[idx]].push_back(word);
            idx++;
        }
    }
    
    return data;
}
