//
//  tree.cpp
//  star_cubing
//
//  Created by Atif Ahangar on 3/18/18.
//  Copyright © 2018 Atif Ahangar. All rights reserved.
//

#include "../include/tree.hpp"

extern int mem_cnt;
extern vector<string> dims;
    
Node::Node() {
    count    = 0;
    parent   = NULL;
    sibling  = NULL;
    mem_cnt += sizeof(Node);
}

bool Node::isLeaf() {
    return children.size() == 0;
}

bool Node::hasSiblings() {
    if(this->parent == NULL) return false;
    else {
        long pos = find(this->parent->children.begin(), this->parent->children.end(), this) - this->parent->children.begin();
        return this->parent->children.size()-1 > pos;
    }
}

Node* Node::getSibling() {
    return parent->children.at(distance(parent->children.begin(), find(parent->children.begin(), parent->children.end(), this)) + 1);
}


Tree::Tree() {
    root          = new Node();
    root->count   = 0;
    root->attr    = "root";
    root->parent  = NULL;
    root->sibling = NULL;
    currNode      = root;
    tree_lvl      = 0;
    mem_cnt += sizeof(Tree);
}

Node* Tree::genStarTree(map <vector<string>, int > table_cmpx, vector<string> dims) {
    
    for(map<vector<string>, int>::iterator it = table_cmpx.begin(); it != table_cmpx.end(); ++it) {
        root->count += it->second;
        
        Node* currNode = root;
        Node* tempNode = NULL;
        
        for(int i = 0; i < it->first.size(); i++) {
            int is_present = 0;
            if(currNode->children.size() != 0) {
                for(int j = 0; j < currNode->children.size(); j++) {
                    tempNode = currNode->children[j];
                    if(tempNode->attr == it->first[i] && !tempNode->dimen.compare(dims[i])) {
                        is_present = 1;
                    }
                }
            }
            else {
                is_present = 0;
            }
            
            if(!is_present) {
                Node* newNode = new Node();
                newNode->attr = it->first[i];
                newNode->count = it->second;
                newNode->dimen = dims[i];
                newNode->parent = currNode;
                currNode->children.push_back(newNode);
                currNode = newNode;
            }
            else {
                currNode = tempNode;
                currNode->count += it->second;
            }
        }
    }
    
    return root;
}

void del_node(Node* N) {
    for(int i = 0; i < N->children.size(); i++) {
        del_node(N->children[i]);
        delete N->children[i];
    }
}

void del_tree(Tree* T) {
    for(int i = 0; i < T->root->children.size(); i++) {
        del_node(T->root->children[i]);
        delete T->root->children[i];
    }
    delete T->root;
}
