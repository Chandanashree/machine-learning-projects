#include <iostream>
#include <string>
#include <map>
#include <set>
#include <functional>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <cstring>
#include <cstdlib>

#include "../include/tree.hpp"
#include "../include/input_proc.hpp"

using std::string;
using std::vector;
using std::cout;
using std::endl;

extern int min_sub;
extern int tree_cnt;
extern int dim_sz;
extern int star_cnt;
extern vector<string> dims;
extern Node* root;

void starCubing(Tree * T, Node * cnode, map<string, int> &output_cbs) {
    
    for(int i = 0; i < T->cuboid_tree.size(); i++) {
        Node* currNode = T->cuboid_tree[i]->currNode;
        Node* tempNode = NULL;
        
        int is_present = 0;
        int done = 0;
        
        if(T->tree_lvl == 2) currNode = T->cuboid_tree[i]->root;
        
        if(T->cuboid_tree[i]->tree_lvl == 0) {
            T->cuboid_tree[i]->tree_lvl++;
            done = 1;
        }
        
        while (done == 0) {
            if(currNode->children.size() != 0) {
                for(int j = 0; j < currNode->children.size() && !done; j++) {
                    tempNode = currNode->children[j];
                    if(!tempNode->attr.compare(cnode->attr) && !tempNode->dimen.compare(cnode->dimen)) {
                        is_present = 1;
                        done = 1;
                    }
                }
            }
            else {
                is_present = 0;
            }
            
            if(!is_present) {
                Node* newNode = new Node();
                newNode->attr = cnode->attr;
                newNode->count = cnode->count;
                newNode->dimen = cnode->dimen;
                newNode->parent = currNode;
                currNode->children.push_back(newNode);
                if(!cnode->hasSiblings()) T->cuboid_tree[i]->currNode = newNode;
                done = 1;
            }
            else {
                if(tempNode->attr.compare("root")) tempNode->count += cnode->count;
                if(!cnode->hasSiblings()) T->cuboid_tree[i]->currNode = tempNode;
            }
        }
    }
    
    Tree * newTree = NULL;
    
    if(cnode->count >= min_sub && cnode->attr.compare("* "))     {
        if(cnode->attr.compare("root")) {
            string cb;
            string cbs;
            Node * temp = cnode;
            while(temp->attr.compare("root")) {
                if(temp->attr.compare("* ")) {
                    cb.insert(0, temp->attr + "(" + temp->dimen + ") ");
                }
                if(!temp->parent->attr.compare("root") && temp->attr.compare("* ") && temp != cnode) {
                    cbs.insert(0, temp->attr + "(" + temp->dimen + ") " + cnode->attr + "(" + cnode->dimen + ") ");
                    output_cbs[cbs] = cnode->count;
                }
                temp = temp->parent;
            }
            output_cbs[cb] = cnode->count;
        }
        else if(cnode->isLeaf()) {
            output_cbs["all"] = cnode->count;
        }
        
        if(!cnode->isLeaf()) {
            newTree = new Tree();
            newTree->root->count = cnode->count;
            newTree->root->parent = NULL;
            newTree->tree_lvl = 0;
            newTree->currNode = newTree->root;
            T->cuboid_tree.push_back(newTree);
        }
    }
    
    if(!cnode->isLeaf()) {
        T->tree_lvl += 1;
        starCubing(T, cnode->children[0], output_cbs);
        T->tree_lvl -= 1;
        for(int i = 0; i < T->cuboid_tree.size(); i++) {
            if(T->cuboid_tree[i]->currNode->parent != NULL) T->cuboid_tree[i]->currNode = T->cuboid_tree[i]->currNode->parent;
        }
    }

    if(newTree != NULL) {
        newTree->tree_lvl--;
        starCubing(newTree, newTree->root, output_cbs);
        del_tree(T->cuboid_tree[T->cuboid_tree.size()-1]);
        delete T->cuboid_tree[T->cuboid_tree.size()-1];
        T->cuboid_tree.pop_back();
    }
    
    if(cnode->hasSiblings()) {
        starCubing(T, cnode->getSibling(), output_cbs);
    }
}

void starCubingBF(map <vector<string>, int > table_cmpx) {
    map <string, int> cuboids;
    
    for(map<vector<string>, int>::iterator it = table_cmpx.begin(); it != table_cmpx.end(); ++it) {
        for(int i = 0; i < it->first.size(); i++) {
            if(it->first[i].compare("* ")) {
                cuboids[it->first[i]] += it->second;
            }
        }
    }
    
    for(map<vector<string>, int>::iterator it = table_cmpx.begin(); it != table_cmpx.end(); ++it) {
        for(map<string, int>::iterator it1 = cuboids.begin(); it1 != cuboids.end(); ++it1) {
            for(map<string, int>::iterator it2 = std::next(it1); it2 != cuboids.end(); ++it2) {
                if(std::find(it->first.begin(), it->first.end(), it1->first) != it->first.end()) {
                   if(std::find(it->first.begin(), it->first.end(), it2->first) != it->first.end()) {
                       string temp = it1->first + " " + it2->first;
                       cuboids[temp] += it->second;
                   }
                }
            }
        }
    }
    
    for(map<vector<string>, int>::iterator it = table_cmpx.begin(); it != table_cmpx.end(); ++it) {
        cuboids["all"] += it->second;
    }
    
    cout << "Output of brute force (only till two attributes at a time and \"all\" cuboid)..." << endl;
    cout << "Will only work with dimensions with distinct attributes..." << endl;
    for(map<string, int>::iterator it = cuboids.begin(); it != cuboids.end(); ++it) {
        cout << "Cuboid: " << it->first << " Count: " << it->second << endl;
    }
    cout << endl;
}
