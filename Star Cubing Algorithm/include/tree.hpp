//
//  tree.hpp
//  star_cubing
//
//  Created by Atif Ahangar on 3/18/18.
//  Copyright © 2018 Atif Ahangar. All rights reserved.
//

#ifndef tree_hpp
#define tree_hpp

#include <iostream>
#include <string>
#include <map>
#include <set>
#include <functional>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <cstring>
#include <cstdlib>

using std::string;
using std::vector;
using std::map;

class Node {
public:
    string         attr;
    string         dimen;
    int            count;
    Node*          parent;
    Node*          sibling;
    vector <Node*> children;
    
    Node();
    
    bool  isLeaf();
    bool  hasSiblings();
    Node* getSibling();
};

class Tree {
public:
    Node*          root;
    Node*          currNode;
    vector<Tree*>  cuboid_tree;
    int            tree_lvl;
    
    Tree();
    
    Node* genStarTree(map <vector<string>, int > table_cmpx, vector<string> dims);
};

void del_node(Node* N);
void del_tree(Tree* T);

#endif /* tree_hpp */
