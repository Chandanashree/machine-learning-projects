//
//  star_cubing.h
//  star_cubing
//
//  Created by Atif Ahangar on 3/18/18.
//  Copyright © 2018 Atif Ahangar. All rights reserved.
//

#ifndef star_cubing_h
#define star_cubing_h

void starCubing(Tree * T, Node * cnode, map<string, int> &output_cbs);
void starCubingBF(map <vector<string>, int > table_cmpx);

#endif /* star_cubing_h */
